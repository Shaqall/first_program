#include <stdio.h>

int main()
  {
   double droga = 0;
    double czas = 0;
    printf ("\n");
    printf ("Witam w programie do obliczania średniej prędkości.\n \n");
    printf ("Podaj drogę w kilometrach i czas w godzinach. (Przedziel je przecinkiem): \n \n");
    scanf ("%lg , %lg", &droga, &czas);
    printf ("\n");
    printf ("Średnia prędkość wynosi: %lg km/h\n \n", droga/czas);
    printf ("(%lg kilometrów przebyte w czasie %lg godzin daje nam średnią prędkość %lg km/h)\n", droga, czas, droga/czas);
    return 0;
  }